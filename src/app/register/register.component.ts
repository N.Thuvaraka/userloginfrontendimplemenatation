import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formdata = { name: "", email: "", password: "" };
  submit = false;
  errorMessage = "";
  loading = false;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.auth.canAuthenticate();
  }

  onSubmit() {

    this.loading = true;

    //call register service
    this.auth
      .register(this.formdata.name, this.formdata.email, this.formdata.password)
      .subscribe({
        next: data => {
          this.router.navigate(['/dashboard']);

        },
        error: data => {
          console.log(data);
          alert("error when register");

          if (data.error.error.message == "INVALID_EMAIL") {



            this.errorMessage = "Invalid Email!";
            alert(this.errorMessage);

          } else if (data.error.error.message == "EMAIL_EXISTS") {

            this.errorMessage = "Already Email Exists!";
            alert(this.errorMessage);


          } else {

            this.errorMessage = "Unknown error occured when creating this account!";
            alert(this.errorMessage);

          }
        }
      }).add(() => {
        this.loading = false;
        console.log('Register process completed!');

      })
  }

}